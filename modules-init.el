;;; modules --- Initialize modules
;;; Commentary: Use a modular emacs config. Generally makes configuring emacs easier to handle; even across multiple computers.

;;; Code: from init.el. Alphabetical order.
(load-user-file "modules/advanced-editing.el") ; Advanced editing extensions like multiple cursors
(load-user-file "modules/autocomp.el") ; Auto-completion
(load-user-file "modules/avy-jump.el") ; Visually jump to stuff
(load-user-file "modules/backups.el") ; How emacs shall handle bakcups
(load-user-file "modules/completion-fw.el") ; Completion framework. Vertico and consult.
(load-user-file "modules/custom-bind.el") ; Custom made keybinds for custom commands/extension
(load-user-file "modules/dirvish.el") ; File manager
(load-user-file "modules/ebooks.el") ; Modes for reading files like .pdf or .epub
(load-user-file "modules/guile.el") ; Guile programming and REPL
(load-user-file "modules/irc.el") ; IRC settings
(load-user-file "modules/lang-support.el") ; Added support for certain languages; for example haskell
(load-user-file "modules/latex.el") ; LaTeX extensions and support
(load-user-file "modules/lisp-programming.el") ; Extensions and support for lisp-dialects
;;(load-user-file "modules/lsp.el") ; Language Server Protocol
(load-user-file "modules/macros.el") ; Save macros
(load-user-file "modules/magit.el") ; Git managment in emacs
(load-user-file "modules/mail.el") ; Mail client in emacs
(load-user-file "modules/modeline.el") ; Customize emacs' modeline
(load-user-file "modules/org.el") ; Extensions and support for org
(load-user-file "modules/pomm.el") ; Pomodoro and third-time technique
(load-user-file "modules/projectile.el") ; Manage projects in emacs
(load-user-file "modules/rendering.el") ; Customize fonts, faces or rendering in emacs
(load-user-file "modules/rss.el") ; RSS feeds
(load-user-file "modules/static-analysis.el") ; flymake
(load-user-file "modules/terminal.el") ; Using shell/terminal in emacs
(load-user-file "modules/theme.el") ; Custom theme
(load-user-file "modules/totp.el") ; Time-based One-Time Password managment in emacs
(load-user-file "modules/ux.el") ; Emacs user experience
(load-user-file "modules/vcs-changes.el") ; Highlight uncommitted changes using VC
(load-user-file "modules/window-man.el") ; How emacs shall handle windows (and buffers)

;;; modules-init.el ends here
