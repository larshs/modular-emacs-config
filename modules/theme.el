;; Theme installation
(use-package solo-jazz-theme
  :ensure t
  :config
  (load-theme 'solo-jazz t))
