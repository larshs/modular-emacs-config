;;; Package --- Completion framework

(use-package vertico
  :ensure t
  :init
  (vertico-mode t)
  (vertico-flat-mode t))

;; Improve directory navigation
(with-eval-after-load 'vertico
  (define-key vertico-map (kbd "RET") #'vertico-directory-enter)
  (define-key vertico-map (kbd "DEL") #'vertico-directory-delete-word)
  (define-key vertico-map (kbd "M-d") #'vertico-directory-delete-char))

;;; Extended completion utilities

(use-package consult
  :ensure t
  :bind (("C-c j" . consult-line)
	 ("C-c i" . consult-imenu))
  :custom
  (global-set-key [rebind switch-to-buffer] #'consult-buffer)
  (setq read-buffer-completion-ignore-case t
	read-file-name-completion-ignore-case t
	completion-ignore-case t))

;;; Ignore ordering with orderless

(use-package orderless
  :ensure t
  :init
  (setq completion-styles '(orderless basic)
	completion-category-overrides '((file (styles basic partial-completion)))))

;;; completion-fw.el ends here
