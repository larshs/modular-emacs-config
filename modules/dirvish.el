;;; Package --- Dirvish
;;; Commentary: File management with dirvish

(use-package dirvish
  :ensure t
  :init
  (dirvish-override-dired-mode)
  :bind (("C-x d" . dirvish)))

;;; dirvish.el ends here
