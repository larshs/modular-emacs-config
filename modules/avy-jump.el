;;; Package --- Avy
;;; Commentary: Jump to arbitrary positions using avy

(use-package avy
  :ensure t
  :bind (("C-c z" . avy-goto-word-1)
	 ("C-c C-l a" . avy-goto-line)))

;; Jump to line
(global-set-key (kbd "C-c C-l g") 'goto-line)

;;; Avy zap to char

(use-package avy-zap
  :ensure t
  :bind (("C-M-z" . avy-zap-to-char)))

;;; avy-jump.el ends here
