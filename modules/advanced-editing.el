;;; Package --- Advanced editing
;;; Summary: enhances editing with advanced features like v-regexp and multiple-cursors

(use-package multiple-cursors
  :ensure t
  :bind (("C-S-c C-S-c" . mc/edit-lines)
	 ("C->" . mc/mark-next-like-this)
	 ("C-<" . mc/mark-previous-like-this)
	 ("C-c C-<" . mc/mark-all-like-this)))

;; Visual Regular Expressions

(use-package visual-regexp
  :ensure t
  :bind (("C-c v r" . vr/replace)
	 ("C-c v q" . vr/query-replace)
	 ;; interface for multiple-cursors
	 ("C-c m" . vr/mc-mark)))

;; Mark multiple similar instances

(use-package mark-multiple
  :ensure t
  :bind (("C-c q n" . mark-next-like-this)
	 ("C-c q a" . mark-all-like-this)))

;; Sudo edit

(use-package sudo-edit
  :ensure t
  :bind (("s-w" . sudo-edit)))

;; Kill-ring

(use-package popup-kill-ring
  :ensure t)

;;; advanced-editing.el ends here
