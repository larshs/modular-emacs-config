;; Time-based One-Time Password

(use-package totp
  :ensure t
  :init
  (setq epg-gpg-program "gpg2")
  :bind (("C-c p k" . totp-copy-pin-as-kill)))


