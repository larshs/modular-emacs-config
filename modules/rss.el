;;; Elfeed base

(use-package elfeed
  :ensure t
  :init
  (setq elfeed-db-directory (expand-file-name "elfeed" user-emacs-directory)
	elfeed-show-entry-switch 'display-buffer)
  :config
  (setq-default elfeed-search-filter "@3-months-ago +unread -ncf "))

;; Elfeed-org

(use-package elfeed-org
  :after elfeed
  :ensure t
  :init
  (setq elfeed-shot-entry-switch 'display-buffer)
  (setq rmh-elfeed-org-files (list "~/.emacs.d/elfeed.org"))

  ;; Auto-update interval.
  (run-at-time nil (* 1 60 60) #'elfeed-org))

;;; Bindings
(defun elfeed-visit ()
  (interactive)
  (find-file "~/.emacs.d/elfeed.org"))
(global-set-key (kbd "C-c e f") 'elfeed-visit)

(global-set-key (kbd "C-x w") 'elfeed)
