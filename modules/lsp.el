;;; LSP Support with EGLOT

(use-package eglot
  :ensure t
  :hook ((c-mode
	  c++-mode) . eglot-ensure))

;;; Enable LSP support in selected languages

;; Scheme (Guile)
;; (add-to-list 'eglot-server-programs `(scheme-mode . ("guile-lsp-server")))
;; (add-hook 'scheme-mode-hook 'eglot-ensure)


