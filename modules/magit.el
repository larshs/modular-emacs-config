;;; Git client

(use-package magit
  :ensure t
  :bind (;; Bind the `magit-status' command to a convenient key.
	 ("C-c g" . magit-status)))

