;;; Paredit

(use-package paredit
  :ensure t
  :blackout t
  :init
  (autoload 'enable-paredit-mode "paredit" "Turn on pseudo-structural editing of Lisp code." t)
  :hook ((emacs-lisp-mode
	  eval-expression-minibuffer-setup
	  ielm-mode
	  lisp-mode
	  lisp-interaction-mode
	  scheme-mode
	  geiser-repl-mode) . enable-paredit-mode)
  :custom
  ;; SLIME REPL
  (add-hook 'slime-repl-mode-hook (lambda () (paredit-mode +1)))

  ;; Stop SLIME's REPL from grabbing DEL,
  ;; which is annoying when backspacing over a '('
  (defun override-slime-repl-bindings-with-paredit ()
    (define-key slime-repl-mode-map
      (read-kbd-macro paredit-backward-delete-key) nil))
  (add-hook 'slime-repl-mode-hook 'override-slime-repl-bindings-with-paredit))

;;; Rainbow delimiters

(use-package rainbow-delimiters
  :ensure t)

;;; Paren-face

(use-package paren-face
  :ensure t
  :hook ((scheme-mode
	  geiser-repl-mode
	  lisp-mode
	  lisp-interaction-mode
	  emacs-lisp-mode
	  ielm-mode) . paren-face-mode))

;;; Agressive Indent

(use-package aggressive-indent
  :ensure t
  :blackout t
  :hook ((scheme-mode
	  lisp-mode
	  lisp-interaction-mode
	  emacs-lisp-mode
	  ielm-mode) . aggressive-indent-mode))
