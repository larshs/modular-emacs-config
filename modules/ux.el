;; Asynchronous processes where possible

(use-package async
  :ensure t
  :init
  (dired-async-mode 1))

;; Dimmer

(use-package dimmer
  :ensure t
  :init
  (dimmer-mode t)
  (dimmer-configure-magit))


;; Cheat-sheet with which-key

(use-package which-key
  :ensure t
  :blackout t
  :init
  (which-key-mode))

;;; SubWordMode
(global-subword-mode 1)

;; Beacon; highlight line upon window-switch

(use-package beacon
  :ensure t
  :blackout t
  :init
  (beacon-mode 1))


;; Change emacs' search with swiper

(use-package swiper
  :ensure t
  :bind (("C-s" . swiper)))

;; Edit init.el
(defun config-visit ()
  (interactive)
  (find-file "~/.emacs.d/init.el"))
(global-set-key (kbd "C-c e e") 'config-visit)

;; Reload init.el
(defun config-reload ()
  (interactive)
  (load-file "~/.emacs.d/init.el"))
(global-set-key (kbd "C-c r") 'config-reload)
