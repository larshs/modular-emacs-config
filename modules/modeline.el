;; Show lines and columns on the modeline
(line-number-mode 1)
(column-number-mode 1)

;;; Clock
;; Currently set to a Japanese format. If desired, change display-time-24hr-format to "t" for 24-hour format, and change display-time-format to your local time format.
(setq display-time-24hr-format nil)
(setq display-time-format "%I:%M%p (%a) %Y年%m月%d日")

;; Turn on clock globally
(display-time-mode 1)

;;; Custom modeline

;; smart-mode-line: sleek custom modeline
;; gives better look wtihout being bloated.

(use-package smart-mode-line
  :ensure t
  :init
  (setq sml/theme 'respectful)
  :config
  (sml/setup))

