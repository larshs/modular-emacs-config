;;; Vterm base

(use-package vterm
  :ensure t
  :init
  ;;; Explain which shell that's in use
  (defvar my-term-shell "/bin/bash")
  (defadvice vterm (before force-bash)
    (interactive (list my-term-shell)))
  (ad-activate 'vterm)
  :config
  (setq vterm-max-scrollback 10000)
  :bind (("<s-return>" . vterm)))

