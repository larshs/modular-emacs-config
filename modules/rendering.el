;;; Japanese font
(defun jpn-font ()
  (face-remap-add-relative 'variable-pitch
                           :family "Noto Sans CJK"
                           :height 1.0))

(add-hook 'elfeed-show-mode-hook 'jpn-font)
(add-hook 'nov-mode-hook 'jpn-font)
(add-hook 'org-mode-hook 'jpn-font)

;;; Line-wrapping
;; org-mode
(add-hook 'org-mode-hook '(lambda () (visual-line-mode 1)))

;; elfeed-show
(add-hook 'elfeed-show-mode-hook '(lambda () (visual-line-mode 1)))
