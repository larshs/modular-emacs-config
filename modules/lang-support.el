;;; Clojure Support

(use-package clojure-mode
  :ensure t)

;;; Elixir Support

(unless (package-installed-p 'elixir-mode)
  (package-install 'elixir-mode))

;;; Go Support

(use-package go-mode
  :ensure t
  :init
  ;; Point at your gopath
  (setenv "GOPATH" "/home/lars/.local/share/golocal"))

;;; Haskell Support

(use-package haskell-mode
  :ensure t)

;;; JSON Support

(use-package json-mode
  :ensure t)

;;; PHP Support

(use-package php-mode
  :ensure t)

;;; Scala Support

(use-package scala-mode
  :ensure t)

;;; Standard ML Support

(use-package sml-mode
  :ensure t)

;;; YAML Support

(use-package yaml-mode
  :ensure t)

;;; Markdown support

(use-package markdown-mode
  :ensure t)
