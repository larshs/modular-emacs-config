;;; PDF reading in emacs

(use-package pdf-tools
  :ensure t
  :init
  (pdf-tools-install)
  (pdf-loader-install))

;;; Open pdf where you last left it

(use-package pdf-view-restore
  :ensure t
  :hook
  (pdf-view-mode . pdf-restore-view-mode)
  :config
  ;; Where pdf-restore information shall be saved
  (setq pdf-view-restore-filename "~/.emacs.d/.pdf-view-restore"))

;;; Novel-mode (.epub)

(use-package nov
  :ensure t
  :config
  (add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode)))
