;; Pomodoro for emacs

(use-package pomm
  :init
  (setq alert-default-style 'libnotify)
  :bind (("C-c C-p t" . pomm-third-time) ; Open transient buffer for "third-time" timeboxing technique
	 ("C-c C-p s" . pomm-start) ; Start
	 ("C-c C-p q" . pomm-stop) ; Stop
	 ("C-c C-p p" . pomm-pause) ; Pause
	 ("C-c C-p r" . pomm-reset) ; Reset
	 ))

