;;; Indication of local VCS changes

(use-package diff-hl
  :ensure t
  :init
  ;; Enable `diff-hl' support by default in programming buffers
  (add-hook 'prog-mode-hook #'diff-hl-mode))
