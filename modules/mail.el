;;; Package --- Mail Management
;;; Commentary: Read and send mail in emacs, using 

;; For non-guix systems
(add-to-list 'load-path "/home/lars/.guix-profile/share/emacs/site-lisp/mu4e/")

(require 'mu4e)

;; SMTP settings:
(setq send-mail-function 'smtpmail-send-it)    ; should not be modified
(setq smtpmail-smtp-server "smtp.migadu.com") ; host running SMTP server
(setq smtpmail-smtp-service 465)               ; SMTP service port number
(setq smtpmail-stream-type 'tls)          ; type of SMTP connections to use

;; Mail folders:
(setq mu4e-drafts-folder "/Drafts")
(setq mu4e-sent-folder   "/Sent")
(setq mu4e-trash-folder  "/Trash")

;; The command used to get your emails (adapt this line, see section 2.3):
(setq mu4e-get-mail-command "mbsync --config ~/.emacs.d/.mbsyncrc lars@larshs.com")
;; Further customization:
(setq mu4e-html2text-command "w3m -T text/html" ; how to hanfle html-formatted emails
      mu4e-update-interval 1800                  ; seconds between each mail retrieval
      mu4e-headers-auto-update t                ; avoid to type `g' to update
      mu4e-view-show-images t                   ; show images in the view buffer
      mu4e-compose-signature-auto-include nil   ; I don't want a message signature
      mu4e-use-fancy-chars t)                   ; allow fancy icons for mail threads

;; Do not reply to yourself:
(setq mu4e-compose-reply-ignore-address '("no-?reply" "	lars@larshs.com"))

;; Set my mail address
(setq user-mail-address "lars@larshs.com")

;; Replace mail keybind
(global-set-key (kbd "C-x m") 'mu4e)

;; Configure desktop notifs for incoming emails:
(use-package mu4e-alert
  :ensure t
  :init
  (defun perso--mu4e-notif ()
    "Display both mode line and desktop alerts for incoming new emails."
    (interactive)
    (mu4e-update-mail-and-index 1)        ; getting new emails is ran in the background
    (mu4e-alert-enable-mode-line-display) ; display new emails in mode-line
    (mu4e-alert-enable-notifications))    ; enable desktop notifications for new emails
  (defun perso--mu4e-refresh ()
    "Refresh emails every 300 seconds and display desktop alerts."
    (interactive)
    (mu4e t)                            ; start silently mu4e (mandatory for mu>=1.3.8)
    (run-with-timer 0 300 'perso--mu4e-notif))
  :after mu4e
  :bind ("<f2>" . perso--mu4e-refresh)  ; F2 turns Emacs into a mail client
  :config
  ;; Mode line alerts:
  (add-hook 'after-init-hook #'mu4e-alert-enable-mode-line-display)
  ;; Desktop alerts:
  (mu4e-alert-set-default-style 'libnotify)
  (add-hook 'after-init-hook #'mu4e-alert-enable-notifications)
  ;; Only notify for "interesting" (non-trashed) new emails:
  (setq mu4e-alert-interesting-mail-query
        (concat
         "flag:unread maildir:/INBOX"
         " AND NOT flag:trashed")))

;;; mail.el ends here
