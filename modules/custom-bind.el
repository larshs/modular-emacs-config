;;; Package --- Custom Keybinds

;; Kill all buffers
(defun kill-all-buffers ()
  (interactive)
  (mapc 'kill-buffer (buffer-list)))
(global-set-key (kbd "C-M-s-k") 'kill-all-buffers)

;; Copy whole line
(defun copy-whole-line ()
  (interactive)
  (save-excursion
    (kill-new
     (buffer-substring
      (point-at-bol)
      (point-at-eol)))))
(global-set-key (kbd "C-c w l") 'copy-whole-line)

;; Kill whole word
(defun kill-whole-word ()
  (interactive)
  (backward-word)
  (kill-word 1))
(global-set-key (kbd "C-c w w") 'kill-whole-word)

;; Copy line and insert below; keep mouse position
(defun cwl-insert-below ()
  (interactive)
  (save-excursion
    (copy-whole-line)
    (end-of-line)
    (newline-and-indent)
    (yank)))
(global-set-key (kbd "C-c w i") 'cwl-insert-below)

;; Copy line and insert above; keep mouse position
(defun cwl-insert-above ()
  (interactive)
  (save-excursion
    (copy-whole-line)
    (beginning-of-line)
    (newline-and-indent)
    (previous-line)
    (yank)))
(global-set-key (kbd "C-c w C-i") 'cwl-insert-above)

;;; custom-bind.el ends here
