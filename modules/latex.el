;;; LaTeX support

(use-package auctex
  :ensure t
  :config
  (setq TeX-auto-save t)
  (setq TeX-parse-self t)
  (setq-default TeX-master nil)
  :hook ((LaTeX-mode-map . LaTeX-math-mode)
	 (LaTeX-mode-map . reftex-mode)))

