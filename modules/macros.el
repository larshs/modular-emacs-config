
(fset 'cusmac-anime1
      (kmacro-lambda-form [?\M-x ?s ?e ?a ?r ?c ?h ?\C-g ?\C-  ?\M-x ?s ?e ?a ?r return ?\( ?\C-d return ?\M-b ?\C-w ?r ?e ?p ?l ?a ?c ?e ?m ?e ?  ?\S-\C-f ?\S-\C-f ?\[ ?\M-x return ?\( ?\C-d return ?\C-b ?\C-  ?\M-x return ?. ?m return ?\C-b ?\C-b ?\C-w backspace ?\C-n ?\C-a ?\C-f ?\C-f] 0 "%d"))

(fset 'cusmac-src
      (kmacro-lambda-form [?# ?+ ?b ?e ?g ?i ?n ?_ ?s ?r ?c return return ?# ?+ ?e ?n ?d ?_ ?s ?r ?c ?\C-p ?\C-p ?\C-e ? ] 0 "%d"))

(fset 'cusmac-addpackage
      (kmacro-lambda-form [?\( ?u ?n ?l ?e ?s ?s ?  ?\( ?p ?a ?c ?k ?a ?g ?e ?- ?i ?n ?s ?t ?a ?l ?l ?e ?d ?- ?p ?  ?\' ?r ?e ?p ?l ?a ?c ?e ?m ?e ?\C-f return ?\( ?p ?a ?c ?k ?a ?g ?e ?- ?r ?e ?f ?r ?e ?s ?h ?- ?c ?o ?n ?t ?e ?n ?t ?s ?\C-f return ?\( ?p ?a ?c ?k ?a ?g ?e ?- ?i ?n ?s ?t ?a ?l ?l ?  ?\' ?r ?e ?p ?l ?a ?c ?e ?m ?e] 0 "%d"))



;; Bind for jumping to this file

(defun macro-conf-visit ()
  (interactive)
  (find-file "~/.emacs.d/modules/macros.el"))
(global-set-key (kbd "C-c e m") 'macro-conf-visit)
