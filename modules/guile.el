;;; Better scheme intergration for emacs. Includes REPL.

(use-package geiser
  :ensure t
  :init
  (defun guile-conf-visit ()
    (interactive)
    (find-file "~/.emacs.d/modules/guile.el"))
  :bind (("C-c e g" . guile-conf-visit)))

(unless (package-installed-p 'geiser-guile)
  (package-install 'geiser-guile))

;;; Use guile scheme
(use-package geiser-guile
  :after geiser
  :ensure t
  :custom
  (setq geiser-default-implementation 'guile)
  (setq geiser-active-implementations '(guile))
  (setq geiser-implementations-alist '(((regexp "\\.scm$") guile))))

;;(setq geiser-guile-binary "/gnu/store/9wvmd5rflwd8lpykapf0wfzrzxd1x6s6-profile/bin/guile")

