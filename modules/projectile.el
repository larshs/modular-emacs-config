;;; Project helper

(use-package projectile
  :ensure t
  :blackout t
  :init
  (projectile-mode 1)
  :bind (("<f5>" . projectile-compile-project)))

