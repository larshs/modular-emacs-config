(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("05626f77b0c8c197c7e4a31d9783c4ec6e351d9624aa28bc15e7f6d6a6ebd926" "c74e83f8aa4c78a121b52146eadb792c9facc5b1f02c917e3dbb454fca931223" "fee7287586b17efbfda432f05539b58e86e059e78006ce9237b8732fde991b4c" "36ca8f60565af20ef4f30783aa16a26d96c02df7b4e54e9900a5138fb33808da" "c9ddf33b383e74dac7690255dd2c3dfa1961a8e8a1d20e401c6572febef61045" "11cc65061e0a5410d6489af42f1d0f0478dbd181a9660f81a692ddc5f948bf34" "e8567ee21a39c68dbf20e40d29a0f6c1c05681935a41e206f142ab83126153ca" "d9a28a009cda74d1d53b1fbd050f31af7a1a105aa2d53738e9aa2515908cac4c" "06ed754b259cb54c30c658502f843937ff19f8b53597ac28577ec33bb084fa52" "249e100de137f516d56bcf2e98c1e3f9e1e8a6dce50726c974fa6838fbfcec6b" "5a00018936fa1df1cd9d54bee02c8a64eafac941453ab48394e2ec2c498b834a" default))
 '(elfeed-enclosure-default-dir "/home/lars/.local/share/")
 '(package-selected-packages
   '(solo-jazz-theme mu4e-alert 2048-game org-recur blackout smart-mode-line cape use-package avy-zap aggressive-indent ample-theme ample-themes orderless paren-face package-face pomm rainbow-delimiters geiser-guile geiser paredit projectile elfeed-org elfeed vterm totp-widget totp dimmer async nov pdf-view-restore popup-kill-ring sudo-edit mark-multiple visual-regexp multiple-cursors switch-window swiper beacon which-key diff-hl org-contrib magit eglot auctex markdown-mode yaml-mode sml-mode scala-mode php-mode json-mode haskell-mode go-mode elixir-mode clojure-mode consult vertico avy corfu)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :extend nil :stipple nil :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 120 :width normal :foundry "unknown" :family "JuliaMono"))))
 '(elfeed-search-unread-count-face ((t (:foreground "deep sky blue"))))
 '(elfeed-search-unread-title-face ((t (:inherit bold :foreground "#9966ff")))))
